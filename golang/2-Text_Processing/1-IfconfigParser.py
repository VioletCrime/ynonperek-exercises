#!/usr/bin/python

import argparse
from subprocess import Popen, PIPE

def parse_interfaces(output):
    print "Output type: {0}\nOutput: {1}\n\nOutput split: {2}".format(type(output), output, output.split('\n'))
    interface_segments = []
    new_segment = [output[0]]
    print "output type: {0}".format(type(output.split('\n')))
    for line in output.split('\n'):
        if not line[0].isspace():  # New ifaces don't lead with whitespace
            interface_segments.append(new_segment)
            new_segment = [line]
        else:
            new_segment.append(line)

    print "SEGMENTS: {0}".format(interface_segments)

    for segment in interface_segments:
        ifname = segment[0].split(' ')[0]
        inet = ''
        status = ''

        for line in segment:
            if 'inet' in line:
                inet = line.split('inet ')[1].split(' ')[0]
            if 'status:' in line:
                status = line.split('status: ')[1].split(' ')[0]
        yield "{0},{1},{2}".format(ifname, inet, status)

def parse_ifconfig(output):
    result = "interface, inet, status"
    for interface in parse_interfaces(output):
        result += "\n{0}".format(interface)
    print result

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('w', '--write', help="Actually write CSV file, as exercise dictates",
                        default=False, action='store_true', required=False)

if __name__ == "__main__":
    process = Popen(['ifconfig'], stdout=PIPE, stderr=PIPE)
    output, err = process.communicate()
    result = parse_ifconfig(output)
    if args['write']:
        with open('ifconfig_csv', 'w') as phyle:
            phyle.write(result)
    else:
        print result

