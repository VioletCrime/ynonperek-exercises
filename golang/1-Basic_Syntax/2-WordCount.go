package main

import (
    "flag"
    "fmt"
    "io/ioutil"
    "strings"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
    file := flag.String("file", "", "Name of file to process")
    flag.Parse()

    data, err := ioutil.ReadFile(*file)
    check(err)

    data_str := string(data)

    punc_replacer := strings.NewReplacer(",", "", ".", "", ";", "", "\n", "", "-", "", "!", "", "?", "", "(", "", ")", "", "'", "")
    data_str = punc_replacer.Replace(data_str)
    data_str = strings.ToLower(data_str)

    words := strings.Fields(data_str)

    wordmap := make(map[string]int)

    highest_count := 0
    var highest_count_words []string

    for _, word := range words {
        wordmap[word] = wordmap[word] + 1
        if wordmap[word] > highest_count {
            highest_count = wordmap[word]
            highest_count_words = highest_count_words[:0]
            highest_count_words = append(highest_count_words, word)
        } else if wordmap[word] == highest_count {
            highest_count_words = append(highest_count_words, word)
        }
    }
    fmt.Printf("Word(s) with the highest count (%v) in the file '%v' is/are: ", highest_count, *file)
    var wordout string
    for i, word := range highest_count_words {
        if i != 0 {
            wordout = ", '%v'"
        } else {
            wordout = "'%v'"
        }
        fmt.Printf(wordout, word)
    }
    fmt.Printf("\n")
    //fmt.Printf("wordmap: \n%v", wordmap)
}
