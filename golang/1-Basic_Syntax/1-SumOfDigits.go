package main

import(
    "flag"
    "fmt"
    )

func main() {
    orig_value := flag.Int("val", 123, "Integer to sum the digits of")
    flag.Parse()
    sum := 0
    value := *orig_value
    for value > 0 {
        sum += value % 10
        value = value / 10
    }
    fmt.Printf("The sum of '%v's digits is: %v\n", *orig_value, sum)
}
