#!/usr/bin/python

import argparse
import os

def run_count(filename):
    with open(filename) as phyle:
         lines = phyle.readlines()
    result = {}
    for line in lines:
        for word in line.split(' '):
            if word in ['', '\n'] or word.isspace():
                continue
            word = word.replace('\n', '').replace('.', '').replace(',', '').lower()
            result[word] = result.get(word, 0) + 1
    return result

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', help="Name of file to run wordcount on", required=True)
    parser.add_argument('-a', '--all', help="Print full list of words counted", required=False,
                        default=False, action='store_true')
    return vars(parser.parse_args())

if __name__ == "__main__":
    args = parse_args()

    wordcounts = run_count(args['file'])
    sortedwords = sorted(wordcounts, key=wordcounts.get, reverse=True)
    #print "The word with the highest count is '{0}' ({1} instances)".format(wordcounts[0][0], wordcounts[0][1])
    if not args['all']:
        print "'{0}' is the word which appears most in '{1}', with {2} occurrences.".format(sortedwords[0], args['file'], wordcounts[sortedwords[0]])
    else:
        for word in sortedwords:
            print "'{0}' : {1}".format(word, wordcounts[word])


