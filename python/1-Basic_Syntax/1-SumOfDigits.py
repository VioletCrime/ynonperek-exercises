#!/usr/bin/python

import sys
import unittest

def print_usage(rc):
    print "usage: {0} <integer>".format(sys.argv[0])
    sys.exit(rc)


def sumDigits(toSum):
    running_total = 0
    while toSum > 0:
        running_total += toSum % 10
        toSum = toSum / 10
    return running_total


class TestSumDigits(unittest.TestCase):
    def test_sum_digits(self):
        tests = [
                    [1, 1],
                    [9, 9],
                    [0, 0],
                    [10, 1],
                    [99, 18],
                    [123, 6]
                ]

        for testpair in tests:
            self.assertEqual(sumDigits(testpair[0]), testpair[1])


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print_usage(1)
    if sys.argv[1] in ['-h', '--help']:
        print_usage(0)
    if sys.argv[1] in ['-t', '--test']:
        sys.argv = [sys.argv[0]]
        unittest.main()
    print "Sum of digits: {0}".format(sumDigits(int(sys.argv[1])))
